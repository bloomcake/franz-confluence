# Franz Recipe Confluenece

Use your Confluence Cloud or Server in Franz.

### About

* Only for Atlassian Confluence Cloud or Confluence Server (self-hosted)
* Version 1.0.0

### How do I get set up? (Linux)
* cd ~/.config/Franz/recipes/dev
* (Note that this dev directory may not exist yet, and you must create it `mkdir -p ~/.config/Franz/recipes/dev`)
* Clone this repository `git clone https://gitlab.com/bloomcake/franz-confluence.git`
* Restart Franz and add new Service
